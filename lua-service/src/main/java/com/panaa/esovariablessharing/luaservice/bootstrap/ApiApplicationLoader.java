package com.panaa.esovariablessharing.springapi.bootstrap;
import com.panaa.esovariablessharing.springapi.entity.Account;
import com.panaa.esovariablessharing.springapi.repository.AccountRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Component;

@Component
public class ApiApplicationLoader implements ApplicationListener<ContextRefreshedEvent> {


    private AccountRepository accountRepository;


    private static final Logger LOGGER = LoggerFactory.getLogger(ApiApplicationLoader.class);

    @Autowired
    public void setTicketRepository(AccountRepository accountRepository) {
        this.accountRepository = accountRepository;
    }

    @Override
    public void onApplicationEvent(ContextRefreshedEvent event) {

        //Account user = new Account();
        //accountRepository.save(user);
    }
}
