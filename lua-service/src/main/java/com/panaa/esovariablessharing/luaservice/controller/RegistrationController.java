package com.panaa.esovariablessharing.springapi.controller;

import com.panaa.esovariablessharing.springapi.entity.Account;
import com.panaa.esovariablessharing.springapi.service.AccountService;
import com.panaa.esovariablessharing.springapi.validation.EmailExistsException;
import com.panaa.esovariablessharing.springapi.validation.UsernameExistsException;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
@RequestMapping(value = "/apiv1/registration")
public class RegistrationController {
    private static final Logger LOGGER = LoggerFactory.getLogger(RegistrationController.class);

    @Autowired
    private AccountService accountService;

    @PostMapping(value = "/", produces = "application/json")
    @ApiOperation(value = "", notes = "")
    public ResponseEntity<String> registerUserAccount(@Valid final Account account) {
        LOGGER.debug("Registering user account with information: {}", account);
        try {
            accountService.registerNewUserAccount(account);
        }
        catch (Exception | EmailExistsException | UsernameExistsException e) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(e.getMessage());
        }
        return ResponseEntity.ok().build();
    }
}
