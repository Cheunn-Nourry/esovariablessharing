package com.panaa.esovariablessharing.springapi.entity;

public enum AuthProvider {
    local,
    github,
    google,
    facebook
}
