package com.panaa.esovariablessharing.springapi.entity;

import javax.persistence.*;
import java.util.Collection;

@Entity
class Role {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String name;

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(name = "ACCOUNT_HAS_ROLE", inverseJoinColumns = @JoinColumn(name = "ACCOUNT_ID", referencedColumnName = "ID"),
            joinColumns = @JoinColumn(name = "ROLE_ID", referencedColumnName = "ID"))
    private Collection<Account> accounts;

   /* @ManyToMany
    @JoinTable(
            name = "roles_privileges",
            joinColumns = @JoinColumn(
                    name = "role_id", referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(
                    name = "privilege_id", referencedColumnName = "id"))
    private Collection<Privilege> privileges;*/
}