package com.panaa.esovariablessharing.springapi.repository;

import com.panaa.esovariablessharing.springapi.entity.Account;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AccountRepository extends JpaRepository<Account, Integer> {
    Account findByEmail(String email);

    Account findByUsername(String username);
}
