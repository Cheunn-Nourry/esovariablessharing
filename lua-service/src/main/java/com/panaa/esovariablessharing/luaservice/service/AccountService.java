package com.panaa.esovariablessharing.springapi.service;

import com.panaa.esovariablessharing.springapi.entity.Account;
import com.panaa.esovariablessharing.springapi.validation.EmailExistsException;
import com.panaa.esovariablessharing.springapi.validation.UsernameExistsException;

public interface AccountService {
    void registerNewUserAccount(Account user) throws EmailExistsException, UsernameExistsException; //throws UserAlreadyExistException;

    Account getUser(String verificationToken);

    void saveRegisteredUser(Account user);

    void deleteUser(Account user);

    Boolean emailExists(String email);

    Boolean userNameExist(String username);
}
