package com.panaa.esovariablessharing.springapi.service;

import com.panaa.esovariablessharing.springapi.entity.Account;
import com.panaa.esovariablessharing.springapi.repository.AccountRepository;
import com.panaa.esovariablessharing.springapi.validation.EmailExistsException;
import com.panaa.esovariablessharing.springapi.validation.UsernameExistsException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.UUID;

@Service
public class AccountServiceimpl implements AccountService, UserDetailsService {
    private static final Logger LOGGER = LoggerFactory.getLogger(AccountServiceimpl.class);

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private AccountRepository accountRepository;
    // API

    @Override
    public void registerNewUserAccount(final Account newUser) throws EmailExistsException, UsernameExistsException {
        if (emailExists(newUser.getEmail())) {
            throw new EmailExistsException("There is an account with that email adress: " + newUser.getEmail());
        }
        if (userNameExist(newUser.getUsername())) {
            throw new UsernameExistsException("There is an account with that username : " + newUser.getUsername());
        }
        final Account user = new Account();
        user.setUsername(newUser.getUsername());
        user.setEmail(newUser.getEmail());
        user.setPassword(passwordEncoder.encode(newUser.getPassword()));

        // Generate random string token for email confirmation
        user.setConfirmationToken(UUID.randomUUID().toString());

        LOGGER.debug("Saving user: {}", user.toString());

        accountRepository.save(user);

        LOGGER.debug("Sending confirmation token to: {}", user.getEmail());

        //String message = messages.getMessage("email.registration", null, locale);
        // String link = properties.getRedirectionUrl() + "/emailConfirm?token=" + user.getConfirmationToken();

        //emailService.prepareAndSend(user.getEmail(), properties.getEmailFrom(), "Registration confirmation", message, link);
    }


    @Override
    public Account getUser(final String verificationToken) {
        return null;
    }

    @Override
    public void saveRegisteredUser(Account user) {

    }

    @Override
    public void deleteUser(Account user) {

    }

    @Override
    public Boolean emailExists(String email) {
        return accountRepository.findByEmail(email) != null;
    }

    @Override
    public Boolean userNameExist(String username) {
        return accountRepository.findByUsername(username) != null;
    }

    public UserDetails loadUserByUsername(String userId) throws
            UsernameNotFoundException {
        Account user = accountRepository.findByUsername(userId);
        if(user == null){
            throw new UsernameNotFoundException("Invalid username or password.");
        }
        return new org.springframework.security.core.userdetails.User(
                user.getUsername(), user.getPassword(), user.getAuthorities());
    }
}
