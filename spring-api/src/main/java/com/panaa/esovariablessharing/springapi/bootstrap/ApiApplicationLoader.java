package com.panaa.esovariablessharing.springapi.bootstrap;
import com.panaa.esovariablessharing.springapi.entity.Account;
import com.panaa.esovariablessharing.springapi.repository.AccountRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Component;
import  com.panaa.esovariablessharing.springapi.entity.Role;

import java.util.ArrayList;
import java.util.Collection;

@Component
public class ApiApplicationLoader implements ApplicationListener<ContextRefreshedEvent> {


    @Autowired
    private AccountRepository accountRepository;


    private static final Logger LOGGER = LoggerFactory.getLogger(ApiApplicationLoader.class);


    @Override
    public void onApplicationEvent(ContextRefreshedEvent event) {
       // this.createUsers("admin", "ADMIN");
        //this.createUsers("test", "DEVELOPER");
         //this.createUsers("user", "USER");
    }


    private void createUsers(String name, String roleN){
        Role role = new Role();
        role.setId((long) 1);
        role.setName(roleN);
        Collection<Role> roles = new ArrayList<>();
        roles.add(role);


        Account user = new Account();
        user.setId(999);
        user.setUsername(name);
        user.setPassword(name);
        user.setEmail(name + "@gmail.com");
        user.setEmailVerified(true);
        user.setEnabled(true);
        user.setRoles(roles);

        accountRepository.save(user);
    }
}
