package com.panaa.esovariablessharing.springapi.constant;

public class MicroServiceConstants {

    public static String BASE_API = "/api";

    public interface AdminMicroServiceConstants {
        String BASE = "admin-service";
        String SEARCH_ADMIN = "/search";
        String UPDATE_ADMIN = "/update";
    }
}
