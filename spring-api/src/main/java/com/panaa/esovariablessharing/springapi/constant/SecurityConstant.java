package com.panaa.esovariablessharing.springapi.constant;

public class SecurityConstant{

    public static final String AUTHORIZATION_HEADER = "Authorization";
    public static final String BEARER_PREFIX = "Bearer";
}
