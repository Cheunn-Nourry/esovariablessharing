package com.panaa.esovariablessharing.springapi.controller;

import com.panaa.esovariablessharing.springapi.entity.Account;
import com.panaa.esovariablessharing.springapi.payload.AuthResponse;
import com.panaa.esovariablessharing.springapi.payload.AuthRequest;
import com.panaa.esovariablessharing.springapi.service.AccountService;
import com.panaa.esovariablessharing.springapi.validation.jwt.JwtTokenProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;

@RestController
@RequestMapping("/api/token")
public class AuthenticationController {
    @Autowired
    private AccountService accountService;

    @RequestMapping(value = "/generate-token", method = RequestMethod.POST)
    public ResponseEntity<?> generateToken(@RequestBody AuthRequest loginUser, HttpServletRequest request)
            throws AuthenticationException {
        String token;
        try {
            token = accountService.login(loginUser);
        } catch (Exception e) {
            return ResponseEntity.badRequest().body(e.getMessage());
        }

        return ResponseEntity.ok(new AuthResponse(token));
    }

    @RequestMapping(value = "/me", produces = "application/json", method = RequestMethod.GET)
    public ResponseEntity<?> me(@AuthenticationPrincipal UserDetails user) throws
            UsernameNotFoundException {

        //TODO Implement
        return ResponseEntity.ok(user);
    }
}
