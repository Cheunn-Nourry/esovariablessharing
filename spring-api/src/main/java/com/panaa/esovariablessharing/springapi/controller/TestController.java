package com.panaa.esovariablessharing.springapi.controller;

import com.panaa.esovariablessharing.springapi.entity.Account;
import com.panaa.esovariablessharing.springapi.service.AccountServiceimpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Component;

@Component
public class TestController {
    private static final Logger LOGGER = LoggerFactory.getLogger(TestController.class);

    public boolean check(Integer id, Authentication authentication) {
        LOGGER.info("{}", authentication.getPrincipal());
        return id.equals(authentication.getPrincipal());
    }
}
