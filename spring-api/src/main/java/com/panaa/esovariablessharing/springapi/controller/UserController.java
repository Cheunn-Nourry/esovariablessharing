package com.panaa.esovariablessharing.springapi.controller;

import com.panaa.esovariablessharing.springapi.entity.Account;
import com.panaa.esovariablessharing.springapi.payload.ApiResponse;
import com.panaa.esovariablessharing.springapi.payload.AuthResponse;
import com.panaa.esovariablessharing.springapi.payload.SignUpRequest;
import com.panaa.esovariablessharing.springapi.payload.UpdateUserRequest;
import com.panaa.esovariablessharing.springapi.service.AccountService;
import com.panaa.esovariablessharing.springapi.validation.EmailExistsException;
import com.panaa.esovariablessharing.springapi.validation.PasswordNotMatchException;
import com.panaa.esovariablessharing.springapi.validation.UsernameExistsException;
import com.panaa.esovariablessharing.springapi.validation.jwt.JwtTokenProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.net.URI;

@RestController
@RequestMapping("/api/user")
public class UserController {
    @Autowired
    private AccountService accountService;
    @Autowired
    private TestController testController;
    @RequestMapping(value = "/{id}", method = RequestMethod.PUT)
    //@PreAuthorize("@testController.check(#id, authentication)")
    public ResponseEntity<?> updateUser(@RequestBody UpdateUserRequest updatedUser, @PathVariable Integer id, HttpServletRequest request){
        Account newUser = null;
        try {
            newUser = accountService.updateUser(updatedUser, id);
        } catch (Exception | PasswordNotMatchException e) {
            return ResponseEntity.badRequest().body(e.getMessage());
        }


        return ResponseEntity.ok(newUser);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    @PreAuthorize("#id == authentication.principal.id or hasRole('ROLE_ADMIN')")
    public ResponseEntity<?> deleteUser(@PathVariable Integer id, HttpServletRequest request){
        try {
            accountService.deleteUser(id);
        } catch (Exception e) {
            return ResponseEntity.badRequest().body(e.getMessage());
        }


        return (ResponseEntity<?>) ResponseEntity.ok();
    }


    @PostMapping(value = "/register", produces = "application/json")
    public ResponseEntity<?> registerUserAccount(@RequestBody @Valid final SignUpRequest account) {

        try {
            accountService.registerNewUserAccount(account);
        }
        catch (Exception | EmailExistsException | UsernameExistsException e) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(e.getMessage());
        }
        URI location = ServletUriComponentsBuilder
                .fromCurrentContextPath().path("/user/me")
                .buildAndExpand(1).toUri();

        return ResponseEntity.created(location)
                .body(new ApiResponse(true, "User registered successfully@"));
    }
}
