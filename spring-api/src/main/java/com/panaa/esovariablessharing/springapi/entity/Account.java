package com.panaa.esovariablessharing.springapi.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import javax.persistence.*;
import javax.validation.constraints.Email;
import java.io.Serializable;
import java.util.*;

@Entity
@Table(name = "account", uniqueConstraints = {
        @UniqueConstraint(columnNames = "username"),
        @UniqueConstraint(columnNames = "email")
})

public class Account implements Serializable, UserDetails {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="id")
    private Integer id;

    @Column(nullable=false, unique = true)
    private String username;

    @Column(nullable=false, length = 60)
    @JsonIgnore
    private String password;

    @Email
    @Column(nullable=false, unique = true)
    private String email;

    @Column(nullable=false, name = "email_verified")
    private Boolean emailVerified = false;

    @Column(nullable = false, name = "confirmation_token")
    private String confirmationToken;

    @Column()
    private String avatar;

    @Column(nullable = false)
    private Boolean enabled = false;

    @Column(nullable=false)
    private String default_variable_visibility = "Public";

    @Column(nullable = false, name="IS_USING_2FA")
    private Boolean isUsing2FA = false;

    @CreatedDate
    @Column(nullable=false,name = "created_at")
    private Date createdAt = new Date();

    @LastModifiedDate
    @Column(nullable=false,name = "last_seen")
    private Date lastSeen = new Date();


    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(name = "ACCOUNT_HAS_ROLE", joinColumns = @JoinColumn(name = "ACCOUNT_ID", referencedColumnName = "ID"),
            inverseJoinColumns = @JoinColumn(name = "ROLE_ID", referencedColumnName = "ID"))
    private Collection<Role> roles;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    @Override
    public boolean isAccountNonExpired() {
        return false;
    }

    @Override
    public boolean isAccountNonLocked() {
        return false;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return false;
    }

    @Override
    public boolean isEnabled() {
        return enabled;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {

        List<GrantedAuthority> authorities
                = new ArrayList<>();
        for (Role role: roles) {
            authorities.add(new SimpleGrantedAuthority(role.getName()));
            role.getPrivileges().stream()
                    .map(p -> new SimpleGrantedAuthority(p.getName()))
                    .forEach(authorities::add);
        }

        return authorities;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Boolean getEmailVerified() {
        return emailVerified;
    }

    public void setEmailVerified(Boolean emailVerified) {
        this.emailVerified = emailVerified;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public String getDefault_variable_visibility() {
        return default_variable_visibility;
    }

    public void setDefault_variable_visibility(String default_variable_visibility) {
        this.default_variable_visibility = default_variable_visibility;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Date getLastSeen() {
        return lastSeen;
    }

    public void setLastSeen(Date lastSeen) {
        this.lastSeen = lastSeen;
    }


    public Collection<Role> getRoles() {
        return roles;
    }

    public void setRoles(Collection<Role> roles) {
        this.roles = roles;
    }

    @Override
    public String toString() {
        return "Account{" +
                "id=" + id +
                ", username='" + username + '\'' +
                ", password='" + password + '\'' +
                ", email='" + email + '\'' +
                ", emailVerified=" + emailVerified +
                ", confirmationToken='" + confirmationToken + '\'' +
                ", avatar='" + avatar + '\'' +
                ", enabled=" + enabled +
                ", default_variable_visibility='" + default_variable_visibility + '\'' +
                ", isUsing2FA=" + isUsing2FA +
                ", created=" + createdAt +
                ", lastSeen=" + lastSeen +
                ", roles=" + roles +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Account account = (Account) o;
        return Objects.equals(id, account.id) &&
                Objects.equals(username, account.username) &&
                Objects.equals(password, account.password) &&
                Objects.equals(email, account.email) &&
                Objects.equals(emailVerified, account.emailVerified) &&
                Objects.equals(avatar, account.avatar) &&
                Objects.equals(enabled, account.enabled) &&
                Objects.equals(default_variable_visibility, account.default_variable_visibility) &&
                Objects.equals(createdAt, account.createdAt) &&
                Objects.equals(lastSeen, account.lastSeen) &&
                Objects.equals(roles, account.roles);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, username, password, email, emailVerified, avatar, enabled, default_variable_visibility, createdAt, lastSeen, roles);
    }

    public String getConfirmationToken() {
        return confirmationToken;
    }

    public void setConfirmationToken(String confirmationToken) {
        this.confirmationToken = confirmationToken;
    }

    public Boolean getEnabled() {
        return enabled;
    }

    public void setEnabled(Boolean enabled) {
        this.enabled = enabled;
    }

    public Boolean getUsing2FA() {
        return isUsing2FA;
    }

    public void setUsing2FA(Boolean using2FA) {
        isUsing2FA = using2FA;
    }

}
