package com.panaa.esovariablessharing.springapi.payload;

import javax.validation.constraints.Email;

public class UpdateUserRequest {

    private String username;

    private String password;

    private String newPassword;

    @Email
    private String email;

    private String avatar;

    private Boolean isUsing2FA = false;

    private String default_variable_visibility;

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }

    public String getNewPassword() {
        return newPassword;
    }

    public String getEmail() {
        return email;
    }

    public String getAvatar() {
        return avatar;
    }

    public Boolean getUsing2FA() {
        return isUsing2FA;
    }

    public String getDefault_variable_visibility() {
        return default_variable_visibility;
    }
}
