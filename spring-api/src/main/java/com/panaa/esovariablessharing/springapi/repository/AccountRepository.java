package com.panaa.esovariablessharing.springapi.repository;

import com.panaa.esovariablessharing.springapi.entity.Account;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.PersistenceUnit;

@Repository
@Transactional
public interface AccountRepository extends JpaRepository<Account, Integer> {

    @Query("SELECT a FROM Account a WHERE a.username=:usernameOrEmail OR a.email=:usernameOrEmail")
    Account findByUsernameOrEmail(String usernameOrEmail);

    Account findByEmail(String email);

    Account findByUsername(String username);
}
