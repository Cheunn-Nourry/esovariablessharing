package com.panaa.esovariablessharing.springapi.service;

import com.panaa.esovariablessharing.springapi.entity.Account;
import com.panaa.esovariablessharing.springapi.payload.AuthRequest;
import com.panaa.esovariablessharing.springapi.payload.SignUpRequest;
import com.panaa.esovariablessharing.springapi.payload.UpdateUserRequest;
import com.panaa.esovariablessharing.springapi.validation.EmailExistsException;
import com.panaa.esovariablessharing.springapi.validation.PasswordNotMatchException;
import com.panaa.esovariablessharing.springapi.validation.UsernameExistsException;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

public interface AccountService {
    UserDetails loadUserByUsername(String userId) throws UsernameNotFoundException;

    void registerNewUserAccount(SignUpRequest user) throws EmailExistsException, UsernameExistsException; //throws UserAlreadyExistException;

    String login(AuthRequest login) throws Exception;

    Account getUser(String verificationToken);

    void saveRegisteredUser(Account user);

    void deleteUser(Integer user);

    Account updateUser(UpdateUserRequest user, Integer userID) throws PasswordNotMatchException, Exception;

    Boolean passwordMatch(String oldPassword, String newPassword);

    Boolean emailExists(String email);

    Boolean userNameExist(String username);

}
