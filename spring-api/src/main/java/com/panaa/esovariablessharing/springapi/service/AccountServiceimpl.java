package com.panaa.esovariablessharing.springapi.service;

import com.panaa.esovariablessharing.springapi.entity.Account;
import com.panaa.esovariablessharing.springapi.payload.AuthRequest;
import com.panaa.esovariablessharing.springapi.payload.SignUpRequest;
import com.panaa.esovariablessharing.springapi.payload.UpdateUserRequest;
import com.panaa.esovariablessharing.springapi.repository.AccountRepository;
import com.panaa.esovariablessharing.springapi.validation.EmailExistsException;
import com.panaa.esovariablessharing.springapi.validation.PasswordNotMatchException;
import com.panaa.esovariablessharing.springapi.validation.UsernameExistsException;
import com.panaa.esovariablessharing.springapi.validation.jwt.JwtTokenProvider;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.*;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.Optional;
import java.util.UUID;

@Transactional
@Service
public class AccountServiceimpl implements AccountService, UserDetailsService {
    private static final Logger LOGGER = LoggerFactory.getLogger(AccountServiceimpl.class);

    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private AccountRepository accountRepository;

    @Autowired
    private JwtTokenProvider jwtTokenProvider;


    /**         **
     **         **
     **   API   **
     **         **
     **         **/

    /**
     *
     * @param newUser the body request
     * @throws EmailExistsException
     * @throws UsernameExistsException
     */
    @Override
    public void registerNewUserAccount(final SignUpRequest newUser) throws EmailExistsException, UsernameExistsException {
        LOGGER.debug("Registering user account with information: {}", newUser);
        if (emailExists(newUser.getEmail())) {
            throw new EmailExistsException("There is an account with that email adress: " + newUser.getEmail());
        }
        if (userNameExist(newUser.getUsername())) {
            throw new UsernameExistsException("There is an account with that username : " + newUser.getUsername());
        }
        final Account user = new Account();
        user.setUsername(newUser.getUsername());
        user.setEmail(newUser.getEmail());
        user.setPassword(passwordEncoder.encode(newUser.getPassword()));

        // Generate random string token for email confirmation
        user.setConfirmationToken(UUID.randomUUID().toString());

        LOGGER.info("Saving user: {}", user.toString());

        accountRepository.save(user);

        LOGGER.info("Sending confirmation token to: {}", user.getEmail());

        //String message = messages.getMessage("email.registration", null, locale);
        // String link = properties.getRedirectionUrl() + "/emailConfirm?token=" + user.getConfirmationToken();

        //emailService.prepareAndSend(user.getEmail(), properties.getEmailFrom(), "Registration confirmation", message, link);
    }

    /**
     *
     * @param login the request's body
     * @throws Exception
     */
    @Override
    public String login(AuthRequest login) throws Exception {
        LOGGER.info("LOGIN PROCESS STARTED for user {} ::::", login.getUsername());

        try {
            Authentication authentication = authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(login.getUsername(), login.getPassword()));
            SecurityContextHolder.getContext().setAuthentication(authentication);

        } catch (DisabledException e) {
            throw new Exception("USER_DISABLED", e);
        } catch (BadCredentialsException e) {
            throw new BadCredentialsException("INVALID_CREDENTIALS", e);
        } catch (LockedException e) {
            throw new Exception("LOCKED_USER", e);
        }

        LOGGER.info("LOGIN PROCESS COMPLETED IN ::: " + (new Date()) + " ms");
        return jwtTokenProvider.createToken(login.getUsername(), getUserID(login.getUsername()));
    }

    /**
     *
     * @param userID
     */
    @Override
    public void deleteUser(Integer userID) {

        accountRepository.deleteById(userID);
    }

    @Override
    public Account getUser(final String verificationToken) {
        return null;
    }

    @Override
    public void saveRegisteredUser(Account user) {

    }
    /**
     *
     * @param updateUser request's body
     * @param userID user to update
     * @return The new User's value
     * @throws PasswordNotMatchException
     * @throws Exception
     */
    @Override
    public Account updateUser(UpdateUserRequest updateUser, Integer userID) throws PasswordNotMatchException, Exception {
        LOGGER.info("Updating user with id : {}", userID);
        Account currentUser = null;
        try {
            Optional<Account> user = accountRepository.findById(userID);

            if (user.isEmpty()) throw new Exception("User doesn't exist");

            try {
                currentUser = user.get();
                if (!updateUser.getPassword().isEmpty()) {
                    if (passwordMatch(currentUser.getPassword(), updateUser.getPassword())) {
                        currentUser.setPassword(updateUser.getNewPassword());
                    } else {
                        throw new PasswordNotMatchException("Specified password doesn't match old password");
                    }
                }

                //TODO: if already exists transaction + throw error
                currentUser.setEmail(updateUser.getEmail());
                currentUser.setUsername(updateUser.getUsername());
                currentUser.setUsing2FA(updateUser.getUsing2FA());
                currentUser.setAvatar(updateUser.getAvatar());
                currentUser.setDefault_variable_visibility(updateUser.getDefault_variable_visibility());


                accountRepository.save(currentUser);
            } catch (Exception e) {
                throw e;
            }
        } catch (Exception e) {
            throw e;
        }
        return currentUser;
    }


    /**         **
     **         **
     **  UTILS  **
     **         **
     **         **/

    @Override
    public Boolean passwordMatch(String oldPassword, String specifiedPassword) {
        return oldPassword.equals(specifiedPassword);
    }

    @Override
    public Boolean emailExists(String email) {
        return accountRepository.findByEmail(email) != null;
    }

    @Override
    public Boolean userNameExist(String username) {
        return accountRepository.findByUsername(username) != null;
    }


    /**
     * Get userID from username or email from DB
     * because spring security doesn't allow you to get user's ID
     * @param username
     * @return user's ID
     */
    private Integer getUserID(String username) {
        //TODO add trycatch
        return accountRepository.findByUsernameOrEmail(username).getId();
    }
    /**
     * Basic spring security implementation, check spring DOC
     * https://www.baeldung.com/spring-security-zuul-oauth-jwt
     * @param userId
     * @return
     * @throws UsernameNotFoundException
     */
    @Override
    public UserDetails loadUserByUsername(String userId) throws
            UsernameNotFoundException {
        Account account = accountRepository.findByUsernameOrEmail(userId);

        if (account == null) {
            throw new UsernameNotFoundException("User '" + userId + "' not found");
        }
        return org.springframework.security.core.userdetails.User
                .withUsername(String.join("-",account.getUsername(), account.getEmail()))//
                .authorities(account.getAuthorities())
                .password(account.getPassword())//
                .accountExpired(false)//
                .accountLocked(false)//
                .credentialsExpired(false)//
                .disabled(false)//
                .build();
    }
}
