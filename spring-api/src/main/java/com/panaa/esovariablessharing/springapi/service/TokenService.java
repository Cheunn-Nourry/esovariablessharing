package com.panaa.esovariablessharing.springapi.service;

public interface TokenService {
    String doGenerateToken(String subject);
}
