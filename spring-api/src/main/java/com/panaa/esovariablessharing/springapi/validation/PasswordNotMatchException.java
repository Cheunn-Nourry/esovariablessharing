package com.panaa.esovariablessharing.springapi.validation;

public class PasswordNotMatchException extends Throwable {

    public PasswordNotMatchException(final String message) {
        super(message);
    }

}
