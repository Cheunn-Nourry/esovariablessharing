package com.panaa.esovariablessharing.springapi.validation.jwt;


import com.panaa.esovariablessharing.springapi.service.AccountService;
import io.jsonwebtoken.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.servlet.http.HttpServletRequest;
import java.util.Base64;
import java.util.Date;
import java.util.Objects;

import static com.panaa.esovariablessharing.springapi.constant.JWTConstants.JWT_KEY;
import static com.panaa.esovariablessharing.springapi.constant.SecurityConstant.AUTHORIZATION_HEADER;
import static com.panaa.esovariablessharing.springapi.constant.SecurityConstant.BEARER_PREFIX;


@Component
public class JwtTokenProvider {
    String DEVELOPER_MESSAGE= "Request not authorized.";
    String MESSAGE ="Unmatched JWT token.";
    private static final Logger LOGGER = LoggerFactory.getLogger(JwtTokenProvider.class);

    @Autowired
    private JwtProperties jwtProperties;
    @Autowired
    private AccountService accountService;

    private String secretKey;

    @PostConstruct
    protected void init() {

        secretKey = Base64.getEncoder().encodeToString(jwtProperties.getSecretKey().getBytes());
    }

    /**
     * Create JWT Token with user_id and userdetails
     * @param username
     * @param userID
     * @return
     */
    public String createToken(String username, Integer userID) {
        /*NetworkResponseDTO network = NetworkUtils.getDeviceAddresses.apply(request);*/
        Claims claims = Jwts.claims().setSubject(username);
        claims.put("user_id", userID);
        return Jwts.builder()
                .setClaims(claims)
                //.claim("mac", network.getMacAddress())
                //.claim("ip", network.getIpAddress())
                .setIssuer(JWT_KEY)
                .setExpiration(calculateExpirationDate())
                .signWith(SignatureAlgorithm.HS256, secretKey)
                .compact();
    }

    private Date calculateExpirationDate() {
        Date now = new Date();
        return new Date(now.getTime() + jwtProperties.getValidityInMilliseconds());
    }

    /**
     * Retrieve userdetails from db
     * @param token
     * @return
     */
    public Authentication getAuthentication(String token) {
        UserDetails accountService = this.accountService.loadUserByUsername(getUsername(token));
        return new UsernamePasswordAuthenticationToken(accountService, "", accountService.getAuthorities());
    }

    /**
     *
     * @param token
     * @return
     */
    private String getUsername(String token) {
        return Jwts.parser().setSigningKey(secretKey).parseClaimsJws(token).getBody().getSubject();
    }

    /**
     *
     * @param token
     * @return
     */
    private String getUserId(String token) {
        //TODO Check if Object is somewhat of a string
        return Jwts.parser().setSigningKey(secretKey).parseClaimsJws(token).getBody().get("user_id").toString();
    }

    /**
     * Basic implementation of token retrieving
     * @param req
     * @return
     */
    public String resolveToken(HttpServletRequest req) {
        String bearerToken = req.getHeader(AUTHORIZATION_HEADER);

        return (!Objects.isNull(bearerToken) && bearerToken.startsWith(BEARER_PREFIX)) ?
                bearerToken.substring(7, bearerToken.length()) : null;
    }

    /**
     * Check if token is still valid
     * @param token
     * @return
     * @throws Exception
     */
    public boolean validateToken(String token) throws Exception {

        try {
            Jws<Claims> claims = Jwts.parser()
                    .setSigningKey(secretKey)
                    .parseClaimsJws(token);

            return (!claims.getBody().getExpiration().before(new Date()));
        } catch (JwtException | IllegalArgumentException e) {
            LOGGER.error("Expired or invalid JWT token");
            //throw new Exception(MESSAGE);
            throw new Exception("message");
        }
    }
}
